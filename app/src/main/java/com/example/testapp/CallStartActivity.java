package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class CallStartActivity extends AppCompatActivity {
TextView dialingTextView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_call_start);

        dialingTextView = findViewById(R.id.dialingTextView);

        dialingTextView.setText("Dialing...");

    }
}