package com.example.testapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.media.MediaPlayer;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity implements MyRecyclerViewAdapter.ItemClickListener {
    LinearLayout NumberEnteredShownLayout;
    LinearLayout oneLayout, twoLayout, threeLayout, fourLayout, fiveLayout, sixLayout, sevenLayout, eightLayout, nineLayout, zeroLayout, astricLayout, hashLayout;
    TextView dialedNumberTextView, recentTextView, contactsTextView;
    ImageView backButtonImageView;
    MediaPlayer mediaPlayer;
    MyRecyclerViewAdapter adapter;
    TextView airtelTextView, otherSimTextView;
    EditText searchContactEditText;
    Intent i;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


        setContentView(R.layout.activity_main);
        i = new Intent();
        NumberEnteredShownLayout = findViewById(R.id.NumberEnteredShownLayout);
        oneLayout = findViewById(R.id.oneLayout);
        twoLayout = findViewById(R.id.twoLayout);
        threeLayout = findViewById(R.id.threeLayout);
        fourLayout = findViewById(R.id.fourLayout);
        fiveLayout = findViewById(R.id.fiveLayout);
        sixLayout = findViewById(R.id.sixLayout);
        sevenLayout = findViewById(R.id.sevenLayout);
        eightLayout = findViewById(R.id.eightLayout);
        nineLayout = findViewById(R.id.nineLayout);
        zeroLayout = findViewById(R.id.zeroLayout);
        astricLayout = findViewById(R.id.astricLayout);
        hashLayout = findViewById(R.id.hashLayout);

        dialedNumberTextView = findViewById(R.id.dialedNumberTextView);
        backButtonImageView = findViewById(R.id.backButtonImageView);
        airtelTextView = findViewById(R.id.airtelTextView);
        otherSimTextView = findViewById(R.id.otherSimTextView);
        recentTextView = findViewById(R.id.recentTextView);
        contactsTextView = findViewById(R.id.contactsTextView);
        searchContactEditText = findViewById(R.id.searchContactEditText);


        searchContactEditText.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(MainActivity.this, "In this section, we can search a contact by Name", Toast.LENGTH_LONG).show();
            }
        });

        contactsTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contactsTextView.setTextColor(Color.BLUE);
                recentTextView.setTextColor(Color.WHITE);
                Toast.makeText(MainActivity.this, "On clicking Contacts, all contacts screen will open", Toast.LENGTH_LONG).show();
            }
        });

        recentTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                contactsTextView.setTextColor(Color.WHITE);
                recentTextView.setTextColor(Color.BLUE);

            }
        });


        airtelTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                i.setClass(MainActivity.this, CallStartActivity.class);
                if (NumberEnteredShownLayout.getVisibility() != View.VISIBLE) {
                    NumberEnteredShownLayout.setVisibility(View.VISIBLE);
                    dialedNumberTextView.setText("9718037309");
                } else if (NumberEnteredShownLayout.getVisibility() == View.VISIBLE) {
                    Toast.makeText(MainActivity.this, "Dialing " + dialedNumberTextView.getText(), Toast.LENGTH_LONG).show();
                    startActivity(i);
                }

            }
        });
        otherSimTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (NumberEnteredShownLayout.getVisibility() != View.VISIBLE) {
                    NumberEnteredShownLayout.setVisibility(View.VISIBLE);
                    dialedNumberTextView.setText("9718037309");
                } else if (NumberEnteredShownLayout.getVisibility() == View.VISIBLE) {
                    Toast.makeText(MainActivity.this, "Dialing " + dialedNumberTextView.getText(), Toast.LENGTH_LONG).show();
                    startActivity(i);
                }

            }
        });


        mediaPlayer = MediaPlayer.create(this, R.raw.key_tone);

        oneLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addDigit("1");
            }
        });

        twoLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addDigit("2");
            }
        });
        threeLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addDigit("3");
            }
        });
        fourLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addDigit("4");
            }
        });
        fiveLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addDigit("5");
            }
        });
        sixLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addDigit("6");
            }
        });
        sevenLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addDigit("7");
            }
        });
        eightLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addDigit("8");
            }
        });
        nineLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addDigit("9");
            }
        });
        zeroLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addDigit("0");
            }
        });
        astricLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addDigit("*");
            }
        });
        hashLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                addDigit("#");
            }
        });

        ArrayList<Contact> contactsList = new ArrayList<>();
        Contact contact1 = new Contact();
        Contact contact2 = new Contact();
        Contact contact3 = new Contact();
        Contact contact4 = new Contact();
        Contact contact5 = new Contact();
        Contact contact6 = new Contact();
        Contact contact7 = new Contact();
        Contact contact8 = new Contact();
        Contact contact9 = new Contact();


        contact1.setContactName("Bilal Khan");
        contact1.setContactNumber("+91 9718037309 Incoming call 8m 29s");
        contactsList.add(contact1);

        contact2.setContactName("Chetan Aricent");
        contact2.setContactNumber("+91 9080908990 Incoming call 3m 20s");
        contactsList.add(contact2);

        contact3.setContactName("Yogendra Aricent");
        contact3.setContactNumber("+91 1212121212 Incoming call 12m 29s");
        contactsList.add(contact3);

        contact4.setContactName("Ashish Sir");
        contact4.setContactNumber("+91 9020303040 Incoming call 3m 29s");
        contactsList.add(contact4);

        contact5.setContactName("Ridhima");
        contact5.setContactNumber("+91 9123456780 Incoming call 8m 49s");
        contactsList.add(contact5);

        contact6.setContactName("Anshul Agarwal");
        contact6.setContactNumber("+91 9718037309 Incoming call 20m 29s");
        contactsList.add(contact6);

        contact7.setContactName("Bilal Khan");
        contact7.setContactNumber("+91 9718037309 Incoming call 8m 29s");
        contactsList.add(contact7);

        contact8.setContactName("Javed Khan");
        contact8.setContactNumber("+91 9718037309 Incoming call 5m 29s");
        contactsList.add(contact8);

        contact9.setContactName("Malak Khan");
        contact9.setContactNumber("+91 9718037309 Incoming call 24m 17s");
        contactsList.add(contact9);

        // set up the RecyclerView
        RecyclerView recyclerView = findViewById(R.id.contactsRecyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        adapter = new MyRecyclerViewAdapter(this, contactsList);
        adapter.setClickListener(this);
        recyclerView.setAdapter(adapter);


        backButtonImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mediaPlayer.start();
                if (dialedNumberTextView.getText().length() != 0) {
                    dialedNumberTextView.setText(dialedNumberTextView.getText().toString().substring(0, dialedNumberTextView.getText().length() - 1));
                }
                if (dialedNumberTextView.getText().length() == 0) {
                    NumberEnteredShownLayout.setVisibility(View.GONE);
                }
            }
        });


    }


    @Override
    public void onItemClick(View view, int position) {
        Toast.makeText(this, "Dialing " + adapter.getItem(position).getContactName(), Toast.LENGTH_SHORT).show();
        i.setClass(MainActivity.this, CallStartActivity.class);
        startActivity(i);
    }

    void addDigit(String numberPressed) {
        mediaPlayer.start();
        if (dialedNumberTextView.getText().length() == 0) {
            NumberEnteredShownLayout.setVisibility(View.VISIBLE);
        }

        dialedNumberTextView.setText(dialedNumberTextView.getText() + numberPressed);

    }
}